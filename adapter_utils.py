import requests

def log_error(connector, status, message, result=None):
    if result is None:
        result = {}
    result["status"] = status
    result["level"] = "ERROR"
    result["message"] = message
    connector.sendLog(result)
    return result

def get_supported_languages(languages_endpoint_basedomain):
    supported_languages_response = requests.get(
        f"{languages_endpoint_basedomain}/supported-languages"
    )
    if supported_languages_response != 200:
        supported_languages_response.raise_for_status()
    return supported_languages_response.json()

def get_translation_targets(languages_endpoint_basedomain):
    translation_targets_response = requests.get(
        f"{languages_endpoint_basedomain}/translation-targets"
    )
    if translation_targets_response != 200:
        translation_targets_response.raise_for_status()
    return translation_targets_response.json()